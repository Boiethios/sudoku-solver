use std::error::Error;
use std::fmt;

#[derive(Debug)]
struct SudokuSolver {
    grid: Grid,
}

#[derive(Debug, Default)]
struct Grid([[u8; 9]; 9]);

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        for row in &self.0 {
            for num in row {
                write!(f, "{} ", num)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl std::str::FromStr for SudokuSolver {
    type Err = Box<Error>;
    
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut grid = Grid::default();
        
        for (c, cell) in s.chars().zip(grid.0.iter_mut().flat_map(|row| row.iter_mut())) {
            *cell = match c {
                c @ '1'...'9' => c.to_digit(10).ok_or(GridError)? as u8,
                '.'           => 0,
                _             => continue,
            }
        }
        Ok(SudokuSolver{ grid })
    }
}

impl SudokuSolver {
    pub fn grid(&self) -> &Grid {
        &self.grid
    }
    
    pub fn solve(&mut self) -> Result<(), String> {
        match Self::backtracking_solver(&mut self.grid, 0) {
            true  => Ok(()),
            false => Err("Grid has no solution".into()),
        }
    }
    
    fn backtracking_solver(grid: &mut Grid, current: usize) -> bool {
        if current == 81 {
            return true
        }
        
        let (y, x) = (current / 9, current % 9);
        
        if grid.0[y][x] != 0 {
            return Self::backtracking_solver(grid, current + 1);
        }
        for i in 1..10 {
            grid.0[y][x] = i;
            if Self::is_grid_valid(&grid.0, y, x) && Self::backtracking_solver(grid, current + 1) {
                return true;
            }
        }
        grid.0[y][x] = 0;
        false
    }
    
    fn is_grid_valid(grid: &[[u8; 9]], y: usize, x: usize) -> bool {
        let line = &grid[y];
        let column = grid.iter().map(|row| row[x]);
        let square = grid.chunks(3).nth(y / 3).unwrap().iter()
                         .flat_map(|rows| rows.chunks(3).nth(x / 3).unwrap());

        has_unique_elements(line.iter().filter(|&x| *x != 0))
        && has_unique_elements(column.filter(|x| *x != 0))
        && has_unique_elements(square.filter(|&x| *x != 0))
    }
}


fn main() {
    use std::str::FromStr;
    
    let input = "\
        482.....6\
        ..9......\
        .....24.7\
        .78..4.6.\
        ...3.5...\
        .3.1..72.\
        1.54.....\
        ......2..\
        2.....914";
    let mut solver = SudokuSolver::from_str(input).unwrap();
    println!("{}", solver.grid());
    match solver.solve() {
        Ok(_)    => println!("{}", solver.grid()),
        Err(msg) => println!("{}", msg),
    }

}

/* Error type */

#[derive(Debug)]
struct GridError;

impl fmt::Display for GridError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Cannot parse grid")
    }
}

impl Error for GridError {
    fn description(&self) -> &str {
        "Could not parse sudoku grid"
    }
}

/* Check unique element */

use std::collections::HashSet;
use std::hash::Hash;

fn has_unique_elements<T>(iter: T) -> bool
where
    T: IntoIterator,
    T::Item: std::fmt::Debug + Eq + Hash,
{
    let mut uniq = HashSet::new();
    iter.into_iter().all(move |x| uniq.insert(x))
}
